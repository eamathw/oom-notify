#!/bin/env python3

import os
import re
import sys
import subprocess
import pwd
import smtplib
import syslog
import configparser
import argparse
from email.mime.text import MIMEText
from subprocess import Popen, PIPE

# Used only for testing:
TEST_UID = None # numerical UID or None to disable

def main():
    global config
    global args
    
    parser = argparse.ArgumentParser()
    parser.add_argument('-c','--config', help='Configuration file',required=True)
    args = parser.parse_args()

    if not os.path.exists(args.config):
        abort('Config file not found!')
        
    config = configparser.ConfigParser()

    if not config.read(args.config):
        abort('Unable to read config!')
        
    syslog.openlog('oom-notify')

    if config['general']['verbose']:
        syslog.syslog('OOM Notify starting')

    for line in sys.stdin:
        do_stuff(line)
        
    if config['general']['verbose']:
        syslog.syslog('OOM notify finished')


def abort(msg):
   sys.stderr.write('Aborting: %s'%msg)
   syslog.syslog(msg)        
   exit(1)


def do_stuff(oomline):
    global config
    
    result = re.search('Killed process (\d+) ', oomline)
    if not result:
        if config['general']['verbose']:
            syslog.syslog('Line does not look like an OOM kill message.')
        exit(0)

    pid = result.group(1)

    process = subprocess.run(
        ["sudo","--", "ausearch", "--input-logs", "--just-one", "-ts", "today", "-p", pid]
        ,stdout=subprocess.PIPE
        ,stderr=subprocess.PIPE
        ,text=True)

    uid=None
    exe=None
    for line in process.stdout.split("\n"):
        # type=SYSCALL msg=audit(1661863160.394:149128): arch=c000003e syscall=59 success=yes exit=0 a0=55b640a35260 a1=55b640a36190 a2=55b640a35af0 a3=8 items=2 ppid=82989 pid=82992 auid=2185 uid=2185 gid=100 euid=2185 suid=2185 fsuid=2185 egid=100 sgid=100 fsgid=100 tty=pts4 ses=1611 comm="tail" exe="/usr/bin/tail" subj=? key=(null)
        result = re.search(r'^type=SYSCALL .+?pid=%s auid=(\d+) .+?exe="([^"]+)"'%(pid),line)
        if result:
            uid = result.group(1)
            exe = result.group(2)
            break

    if TEST_UID is not None:
        uid = TEST_UID
            
    if uid is None:        
        if config['general']['verbose']:
            syslog.syslog('No uid found')
        exit(0)
   
    pwe = pwd.getpwuid(int(uid))
    uname = pwe[0]
    
    to_email = None
    if config['general']['email_lookup'] == 'ldap':
        process = subprocess.run(
            ["ldapsearch","-LLL","-x","uid=%s"%(uname),"mail"]
            ,stdout=subprocess.PIPE
            ,text=True)
        for line in process.stdout.split("\n"):
            result = re.search(r'^mail: (.+)',line)
            if result:
                to_email = result.group(1)
                break
    elif config['general']['email_lookup'] == 'local':
        to_email = uname
    else:
        abort('Error: Configuration item "email_lookup" may have an invalid value.')    
        
    if to_email is None:
        abort('No email adress found for %s'%uname)

    hostname = os.uname()[1]

    to = [to_email]

    msg = MIMEText( config['general']['response'].format(
             hostname
             ,pid
             ,exe
             ,oomline)
         )
    msg['From'] = config['general']['smtp_from']
    msg['To'] = to_email
    if 'smtp_bcc' in config['general'].keys() and config['general']['smtp_bcc']:
        msg['BCC'] = config['general']['smtp_bcc']
        to = to + [config['general']['smtp_bcc']]
    msg['Subject'] = config['general']['subject'] 
         
    if config['general']['verbose']:
        syslog.syslog('Sending OOM notification for PID %s to %s'%(pid,to_email))

    if  config['general']['send_via'] == 'sendmail':
        p = Popen([config['general']['sendmail_path'], "-t", "-oi", "-f", config['general']['smtp_from']], stdin=PIPE)     
        p.communicate(msg.as_bytes())    
    elif config['general']['send_via'] == 'localsmtp':
        server = smtplib.SMTP('localhost')
        server.sendmail('root',to,msg.as_bytes())
        server.quit()
    else:
        abort('Error: invalid setting for send_via')
        
        
if __name__ == '__main__':
    main()
    
