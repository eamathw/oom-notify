# About
This script and set of configuration changes watches for syslog messages about
the kernel's Out Of Memory(OOM) killer and sends a notification to the user who
started the process.

This work is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).

# Process
1. The script forks so that rsyslog is not left waiting.
1. Rsyslog watches for messages in the syslog saying that the Out Of Memory(OOM) 
   killer stopped a process due system memory exhaustion. 
1. The oom-notify.py script is run with the matching syslog line as the first
   argument. (The script continues to run and process log lines from rsyslog.)
1. The script queries the auditd logs to get the UID of the user that owned the
   killed process.
1. If configured to do so the script looks up the users email address from LDAP.
   Otherwise it assumes mail sent to the local user will make it to where it
   needs to go.
1. The script sends an email via a local SMTP server or by using sendmail.

# Requirements
* This software was designed to be run on a Debian-like system. Assumptions were
  made about certain programs paths and users.
* Reqires a working SMTP server running on the local host or a sendmail like
  program that can deliver mail
* Sudo 
  
## Packages
* auditd - Works with v3.0.7. [1]
* sudo 
* rsyslog - I am assuming it is standard package in your system
* ldap-utils - If using LDAP lookups for email addresses
* Any package that provides local mail delivery services: exim, postfix, etc.

# Installation
1. apt-get install auditd
1. Edit ./oom-notify.cfg
1. Copy ./oom-notify.cfg somewhere.
1. Copy ./oom-notify.py somewhere.
1. chmod 755 /PATH/TO/oom-notify.py
1. Edit ./etc/rsyslog.d/99-oom-notify.conf and set the location of the config
1. cp ./etc/rsyslog.d/99-oom-notify.conf /etc/rsyslog.d/
1. If you have not modified auditd.conf then copy ./etc/audit/auditd.conf to 
   /etc/audit. Otherwise you will want to merge in your modifications before 
   copying.
1. cp ./etc/audit/rules.d/oom-notify.rules /etc/audit/rules.d/
1. Copy ./etc/sudoers.d/50-oom-notify to /etc/sudoers.d/
1. systemctl enable auditd
1. systemctl start auditd
1. systemctl restart rsyslog

---

* [1] V3.0 does not support the end_of_event_timeout so this should be
  commented out in /etc/audit/auditd.conf
 
